YUI.add('pi-login-view', function (Y) {

var PI = Y.PI

LoginView = Y.Base.create('loginView', Y.View, [], {
    // Compiles the HomePage Template into a reusable Handlebars template.
    template: PI.Templates['login'],
    
    // Attach DOM events for the view. The `events` object is a mapping of
    // selectors to an object containing one or more events to attach to the
    // node(s) matching each selector.
    events: {
        'button': {
            click: 'login'
        },
    
        'input': {
            keypress: 'enter'
        }
    },
    
    initializer: function () {
        // The `login` event is fired when the user submits his login data
        this.publish('login', {preventable: false});
    },
    
    render: function () {
        // Retrieves just the `login` of the `User` model instance and applies
        // it to the HomePage Template.
        var user    = this.get('model'),
            content = this.template({login: ""});
    
        // Adds the "home-page" CSS class to aid styling and sets the resulting
        // HTML as the contents of this view's container.
        this.get('container').addClass('home-page').setHTML(content);
        Y.log("Hello world!", "info",  "loginView");
        return this;
    },
    
    // Called when the user clicks the "Show Repos" button. This will retrieve
    // the GitHub username from the text `<input>` and fire the `changeUser`
    // event, passing on the username to the app.
    login: function () {
        var user = this.get('container').one('#login-user').get('value');
        var password = this.get('container').one('#login-password').get('value');
        if (user) {
            this.fire('login', {user: user, password: password});
        }
    },
    
    // Called when the user types inside the text `<input>`; when the "enter"
    // key is pressed, this will call the `changeUser()` method.
    enter: function (e) {
        // Check for "enter" keypress.
        if (e.keyCode === 13) {
            this.login();
        }
    }
});

Y.namespace('PI').LoginView = LoginView;

}, '0.1', {
    requires: [
        'view',
        'handlebars',
        'pi-templates'
    ]
});