YUI.add('pi-startpage-view', function (Y) {

var PI = Y.PI;

var StartPageView = Y.Base.create('startPageView', Y.View, [], {
    // Compiles the HomePage Template into a reusable Handlebars template.
    template: Y.Handlebars.compile(Y.one('#t-home').getHTML()),

    // Attach DOM events for the view. The `events` object is a mapping of
    // selectors to an object containing one or more events to attach to the
    // node(s) matching each selector.
    events: {
        'button': {
            click: 'changeUser'
        },

        'input': {
            keypress: 'enter'
        }
    },

    initializer: function () {
        // The `changeUser` event is fired when the user chooses a GitHub user
        // to start browsing. This event will bubble up to the `ContributorsApp`
        // when this view is the app's `activeView`.
        this.publish('changeUser', {preventable: false});
        
        this.loginView = new PI.LoginView();
        // This will cause the sub-views' custom events to bubble up to here.
        this.loginView.addTarget(this);
    },

    render: function () {
        // Retrieves just the `login` of the `User` model instance and applies
        // it to the HomePage Template.
        var user    = this.get('model'),
            content = Y.one(Y.config.doc.createDocumentFragment());

        // This renders each of the two sub-views into the document fragment,
        // then sets the fragment as the contents of this view's container.
        content.append(this.loginView.render().get('container'));
        content.append(this.template(user.getAttrs(['login'])));

        // Adds the "user-page" CSS class to aid styling and sets the document
        // fragment containing the two rendered sub-views as the contents of
        // this view's container.
        this.get('container').addClass('user-page').setHTML(content);

        return this;
        // Adds the "home-page" CSS class to aid styling and sets the resulting
        // HTML as the contents of this view's container.
        this.get('container').addClass('home-page').setHTML(content);

        return this;
    },

    // Called when the user clicks the "Show Repos" button. This will retrieve
    // the GitHub username from the text `<input>` and fire the `changeUser`
    // event, passing on the username to the app.
    changeUser: function () {
        var user = this.get('container').one('input').get('value');
        if (user) {
            this.fire('changeUser', {user: user});
        }
    },

    // Called when the user types inside the text `<input>`; when the "enter"
    // key is pressed, this will call the `changeUser()` method.
    enter: function (e) {
        // Check for "enter" keypress.
        if (e.keyCode === 13) {
            this.changeUser();
        }
    }
});

Y.namespace('PI').StartPageView = StartPageView;

}, '0.1', {
    requires: [
        'view',
        'handlebars',
        'pi-login-view'
    ]
});