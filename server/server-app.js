/******************************************
 * by nenTi
 * ****************************************/

require('./config');

var connect = require('connect'),
    combo   = require('combohandler'),
    express = require('express'),
    YUI     = require('yui').YUI,

    app    = express.createServer(),
    pubDir = global.config.pubDir,
    Y      = YUI();


// -- Express config -----------------------------------------------------------
app.configure('development', function () {
    // Gives us pretty logs in development. Must run before other middleware.
    app.use(express.logger(
        '[:date] :req[x-forwarded-for] ":method :url" :status [:response-time ms]'
    ));
});

app.configure(function () {
    // Don't ignore trailing slashes in routes.
    app.set('strict routing', true);

    // Use our custom Handlebars-based view engine as the default.
    app.set('views', __dirname + '/views');
    app.register('.handlebars', require('./view'));
    app.set('view engine', 'handlebars');

    // Local values that will be shared across all views. Locals specified at
    // render time will override these values if they share the same name.
    app.set('view options', Y.merge(require('./common'), {
        config: global.config
    }));
    

    // Middleware.
    app.use(express.static(pubDir));
    app.use(express.favicon());
    app.use(app.router);
});

app.configure('development', function () {
    app.use(express.errorHandler({
        dumpExceptions: true,
        showStack     : true
    }));
});

app.configure('production', function () {
    // Renable these when minification is done async!
    // app.enable('minify templates');
    // app.enable('minify js');

    app.enable('view cache');
    app.use(express.errorHandler());
});


// -- Routes -------------------------------------------------------------------

// Root.

app.get('/', function (req, res) {
    res.render('index', {
        located: false
    });
});

// Combo-handler for JavaScript.
app.get('/combo', combo.combine({rootPath: pubDir + '/js'}), function (req, res) {
    if (connect.utils.conditionalGET(req)) {
        if (!connect.utils.modified(req, res)) {
            return connect.utils.notModified(res);
        }
    }

    var js = res.body,
        minify;

    if (app.enabled('minify js')) {
        minify = require('uglify-js');
        js     = minify(js);
    }

    res.send(js, 200);
});

// Dymanic resource for precompiled templates.
app.get('/templates.js', function (req, res, next) {
    var precompiled = require('./templates').getPrecompiled(),

        templates = [];

    Y.Object.each(precompiled, function (template, name) {
        templates.push({
            name    : name,
            template: template
        });
    });

    res.render('templates', {
        layout   : false,
        templates: templates
    }, function (err, view) {
        if (err) { return next(); }

        var templates = view,
            minify;

        if (app.enabled('minify templates')) {
            minify    = require('uglify-js');
            templates = minify(view);
        }
module.exports = app;
        res.send(templates, {
            'Content-Type': 'application/javascript'
        }, 200);
    });
});

module.exports = app;